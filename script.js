function nextClue(){
    console.log("Button was clicked successfully");
    var input = document.getElementById("Pass").value;
    let code = input.toString();
    console.log(code);
    console.log("The code inputted was: " + code);

    if (code == "075188") {
        console.log("Code for Clue 2 was inputted");
        location.replace("clue2.html");
    } else if (code == "923767") {
        console.log("Code for Clue 3 was inputted");
        location.replace("clue3.html");
    } else if (code == "494626") {
        console.log("Code for Clue 4 was inputted");
        location.replace("clue4.html");
    } else if (code == "429072") {
        console.log("Code for Clue 5 was inputted");
        location.replace("clue5.html");
    } else if (code == "487556") {
        console.log("Code for Clue 6 was inputted");
        location.replace("clue6.html");
    } else if (code == "808140") {
        console.log("Code for Clue 7 was inputted");
        location.replace("clue7.html");
    } else if (code == "267589") {
        console.log("Code for Clue 8 was inputted");
        location.replace("clue8.html");
    } else if (code == "999591") {
        console.log("Code for Clue 9 was inputted");
        location.replace("clue9.html");
    } else if (code == "861829") {
        console.log("Code for Clue 10 was inputted");
        location.replace("clue10.html");
    } else if (code == "109712") {
        console.log("Code for final code was inputted");
        location.replace("finalcode.html");
    } else {
        alert("That is the wrong code.\nPlease try again!")
    }
}
